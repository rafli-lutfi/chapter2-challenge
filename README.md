# Challenge Chapter-2

## overview
Buatlah sebuah program untuk input nilai siswa dari satu mata pelajaran di kelas, dimana nilai tersebut disimpan dalam sebuah variabel (array). Kemudian keluarkan output setelah selesai menginputkan nilai :

- Nilai tertinggi & terendah

- Mencari rata-rata

- Jumlah Siswa lulus / tidak lulus ujian

- dengan kriteria nilai lulus harus >= 60

- Urutkan nilai siswa

- Cari dan tampilkan siswa nilai=90 dan nilai=100

## flowchart
![flowchart](flowchart.jpg)

## contoh
Inputkan nilai dan ketik “q” jika sudah selesai

100

90

99

59

99

q

Selesai memasukan nilai, cari outputnya :

Nilai tertinggi : 100

Nilai terendah : 89

Siswa lulus : 4, Siswa tidak lulus : 5

Urutan nilai dari terendah ke tertinggi : 59, 90, 99, 99, 100

Siswa nilai 90 dan nilai 100 : 90, 100
