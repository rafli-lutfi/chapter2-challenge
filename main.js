const readline = require("readline-sync");

class Scores {
  constructor(scores) {
    this.scores = scores;
  }

  #BubbleSort() {
    const len = this.scores.length;
    let scores = this.scores;

    for (let i = 0; i < len; i++) {
      for (let j = 0; j < len; j++) {
        if (scores[j + 1] < scores[j]) {
          let temp = scores[j + 1];
          scores[j + 1] = scores[j];
          scores[j] = temp;
        }
      }
    }
    return scores;
  }

  SortedScore() {
    return this.#BubbleSort();
  }

  FindBiggestScore() {
    const sortedScores = this.#BubbleSort();
    return sortedScores[sortedScores.length - 1];
  }

  FindLowestScore() {
    const sortedScores = this.#BubbleSort();
    return sortedScores[0];
  }

  CalcAverageScore() {
    let sum = 0;
    for (let score of this.scores) {
      sum += score;
    }

    return (sum / this.scores.length).toFixed(2);
  }

  StandardMinimumCriteria() {
    const underMinimum = this.scores.filter((score) => {
      return score < 60;
    });

    const aboveMinimum = this.scores.filter((score) => {
      return score >= 60;
    });

    return [underMinimum.length, aboveMinimum.length];
  }

  NinetyAndHundred() {
    const result = [];
    const sortedScore = this.#BubbleSort();

    for (let i = sortedScore.length - 1; i >= 0; i--) {
      let score = sortedScore[i];

      if (score >= 90 || score == 90 || score == 100) result.push(score);
      else break;
    }

    return result;
  }
}

function DisplayResult(scores) {
  const classScore = new Scores(scores);

  const biggest = classScore.FindBiggestScore(),
    lowest = classScore.FindLowestScore(),
    average = classScore.CalcAverageScore(),
    sorted = classScore.SortedScore(),
    [under, above] = classScore.StandardMinimumCriteria(),
    customScore = classScore.NinetyAndHundred();

  console.log(`\nNilai terendah: ${lowest}`);
  console.log(`Nilai tertinggi: ${biggest}`);
  console.log(`Rata-rata: ${average}`);
  console.log(`Siswa lulus : ${above}, Siswa tidak lulus : ${under}`);
  console.log("Urutan nilai dari terendah ke tertinggi: " + sorted);
  console.log("Siswa nilai 90 dan nilai 100: " + customScore);
}

function question(words) {
  return readline.question(words);
}

function main() {
  let repeat = false;

  while (!repeat) {
    const scores = [];

    let input = question("Inputkan nilai dan ketik q jika sudah selesai \n>");
    while (input != "q") {
      let convert = Number(input);

      if (convert != NaN && convert <= 100) scores.push(convert);
      else {
        input = question("bukan angka atau angka yang dimasukkan lebih dari 100 \n>");
        continue;
      }

      input = question(">");
    }
    DisplayResult(scores);

    question("\npress anything...");
    repeat = readline.keyInYN("apakah anda ingin mengakhiri program?");
    console.clear();
  }
}

main();
